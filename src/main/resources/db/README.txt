A dump file is collected from the target system:

mysqldump -u root -p wellnessview_data --hex-blob > dump.sql

A dump file is loaded into a new database within mysql:

create database wellnessview_data;
use wellnessview_data;
source dump.sql
